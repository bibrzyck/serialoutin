import java.io.Serializable;
import java.time.LocalDate;

public abstract class Czlowiek implements Serializable {
private String imie,nazwisko;
private LocalDate dataUrodz;
private static final long serialVersionUID = 1L;
public Czlowiek(){};
public Czlowiek(String imie, String nazwisko, LocalDate dataUrodz) {
	super();
	this.imie = imie;
	this.nazwisko = nazwisko;
	this.dataUrodz = dataUrodz;
}
public String getImie() {
	return imie;
}
public String getNazwisko() {
	return nazwisko;
}
public LocalDate getDataUrodz() {
	return dataUrodz;
}
@Override
public String toString()
{
	return imie+" "+" "+nazwisko+" data ur.: "+dataUrodz.toString();
}
}
