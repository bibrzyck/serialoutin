import java.time.LocalDate;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class MainClass {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Pracownik> l=new LinkedList<Pracownik>();
		l.add(new Pracownik("Jan","Kowalski",LocalDate.of(2000, 2, 23), LocalDate.of(2017, 10, 11)));
		l.add(new Pracownik("Stefan","Mleczko",LocalDate.of(1990, 12, 23), LocalDate.of(2010, 11, 21)));
		Iterator<Pracownik> it=l.iterator();
		while(it.hasNext()){
			System.out.println(it.next().toString());
		}

	}

}
