import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.time.LocalDate;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class SaveObjPracownicy {

	public static void main(String[] args) {
		List<Pracownik> l=new LinkedList<Pracownik>();
		l.add(new Pracownik("Jan","Kowalski",LocalDate.of(2000, 2, 23), LocalDate.of(2017, 10, 11)));
		l.add(new Pracownik("Stefan","Mleczko",LocalDate.of(1990, 12, 23), LocalDate.of(2010, 11, 21)));
		Iterator<Pracownik> it=l.iterator();
		Pracownik tmp;
		try(ObjectOutputStream oos=new ObjectOutputStream(new FileOutputStream("pracownicy.ser"))){
			while(it.hasNext()){
		    tmp=it.next();
			oos.writeObject(tmp);
			}
			System.out.println("Zapis uko�czony");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

}
