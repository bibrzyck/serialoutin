import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;

public class ReadPracownicy {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Pracownik> l=new ArrayList<Pracownik>();
       try(Scanner sc=new Scanner(new FileInputStream("pracownicy.dat"))){
    	  while(sc.hasNextLine())
    	  {
    		  String line=sc.nextLine();
    		  StringTokenizer t=new StringTokenizer(line);
    		  Pracownik tmp=new Pracownik(t.nextToken(), t.nextToken(), LocalDate.parse(t.nextToken()), 
    				  LocalDate.parse(t.nextToken()));
    		  l.add(tmp);
    	  }
    	  Iterator<Pracownik> it=l.iterator();
			while(it.hasNext()){
				System.out.println(it.next().toString());
			}
       } catch (FileNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	}

}
